# Created by Chocofur, Styriam Sp. z o.o.

bl_info = {
    "name": "Chocofur Model Manager",
    "author": "Chocofur",
    "version": (1, 0, 3),
    "blender": (2, 79, 0),
    "location": "View 3D > Tool Shelf",
    "wiki_url": "http://chocofur.com",
    "tracker_url": "http://chocofur.com",
    "support": "COMMUNITY",
    "category": "Add Mesh"
    }    
    
import bpy



# load and reload submodules
##################################    
    
from . import developer_utils
modules = developer_utils.setup_addon_modules(__path__, __name__, "bpy" in locals())

# register
################################## 

import traceback
from . import controller
from extensions_framework import util as efutil

def config_load():
    from . controller import get_default_libpath
    bpy.cmm_config_libpath = efutil.find_config_value(bl_info['name'], 'defaults', 'libpath', get_default_libpath())
    
def register():
    bpy.cmm_config_auto_check_update = efutil.find_config_value(bl_info['name'], 'defaults', 'auto_check_update', False) in ('True', True)
    bpy.cmm_config_updater_intrval_months = int(efutil.find_config_value(bl_info['name'], 'defaults', 'updater_intrval_months', 0))
    bpy.cmm_config_updater_intrval_days = int(efutil.find_config_value(bl_info['name'], 'defaults', 'updater_intrval_days', 7))
    bpy.cmm_config_updater_intrval_hours = int(efutil.find_config_value(bl_info['name'], 'defaults', 'updater_intrval_hours', 0))
    bpy.cmm_config_updater_intrval_minutes = int(efutil.find_config_value(bl_info['name'], 'defaults', 'updater_intrval_minutes', 0))
    
    # addon updater code and configurations
    # in case of broken version, try to register the updater first
    # so that users can revert back to a working version
    from . import addon_updater_ops
    addon_updater_ops.register(bl_info)
    from .addon_updater import Updater as updater
    updater.engine = "bitbucket"
    updater.user = "pierog"
    updater.repo = "chocofur-model-manager"
    updater.addon = "chocofur_model_manager"
    updater.remove_pre_update_patterns = ["*.py","*.pyc"]
    
    from .gui import ChocofurManagerPreferences
    bpy.utils.register_class(ChocofurManagerPreferences)
    
    addon_updater_ops.check_for_update_background()
    
    try: bpy.utils.register_module(__name__)
    except: traceback.print_exc()
    config_load()
    controller.register()
    
    print("Registered {} with {} modules".format(bl_info["name"], len(modules)))
    

def unregister():
    try: bpy.utils.unregister_module(__name__)
    except: traceback.print_exc()
    
    controller.unregister()
    
    print("Unregistered {}".format(bl_info["name"]))
    