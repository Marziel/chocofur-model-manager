import bpy
import os
from time import time
from bpy.props import *
import bpy.utils.previews
from bpy.types import WindowManager
from .gui import cetegory_factory


######################################################################
############################ Library Path ############################
######################################################################
def get_default_libpath():
    script_file = os.path.realpath(__file__)
    dir = os.path.dirname(script_file)
    return os.path.join(dir, 'Models')

def set_libpath(self, value):
    from extensions_framework import util as efutil
    from . import bl_info
    if os.path.isdir(value):
        efutil.write_config_value(bl_info['name'], 'defaults', 'libpath', value)
        bpy.cmm_config_libpath = value
    else:
        print("Chocofur: Invalid path to library")
    return None
    
class PREFERENCES_OT_LibpathSetDefault(bpy.types.Operator):
    bl_idname = "chocofur.libpath_set_default"
    bl_label = "Revert to Default Path"
    bl_options = {'INTERNAL'}

    def execute(self, context):
        set_libpath(self, get_default_libpath())

        return{'FINISHED'}
    
class Lib_Path(bpy.types.Operator):
    bl_idname = "library.path"
    bl_label = "Library Path"
    
    def execute(self, context):
        bpy.ops.wm.path_open(filepath=context.window_manager.chocofur_model_manager.library_path)
        return {'FINISHED'}

##################################################################
########################### Categories ###########################
##################################################################

categories_tmp = []
def list_categories():
    directory = bpy.cmm_config_libpath
    dirs = []
    if os.path.exists(directory):
        for fn in os.listdir(directory):
            if not fn.startswith('.'):
                dirs.append(fn)
    
    global categories_tmp
    categories_tmp = dirs[:]
    
    return dirs
    
class Refresh_Categories(bpy.types.Operator):
    bl_idname = "chocofur.refresh_categories"
    bl_label = "Refresh Categories"
    bl_options = {'INTERNAL'}
    
    def execute(self, context):
        # remove preview collections before adding them again (because categories need to be recreated to insure correct panel order)
        clist = (c['main'] for c in collections.values())
        for c in clist:
            try:
                bpy.utils.previews.remove(c)
            except KeyError:
                print("Preview key error. Ignored.")

        # remove unused categories
        old_categories = categories_tmp[:]
        new_categories = list_categories()
        
        unused_categories = list(set(old_categories) - set(new_categories))
        for c in unused_categories:
            bpy.utils.unregister_class(getattr(bpy.types, "CATEGORY_PT_chocofur_"+c))
            
        for d in new_categories:
            setattr(Chocofur_Model_Manager_WM_Properties, d+'_previews', EnumProperty(
                    items = enum_preview_items_func_factory(d)
            ))
                    
            setattr(Chocofur_Model_Manager_Scene_Properties, d+'_category', EnumProperty(
                name=d+" Category",
                items=populate_category_func_factory(d),
                description="Select "+d,
            ))
            
            pcoll = bpy.utils.previews.new()
            pcoll.previews_dir = ""
            pcoll.previews = ()
            collections[d]={'main': pcoll, 'updated': 0}
            
            bpy.utils.register_class(cetegory_factory(d))
        
        from .gui import options_panel_factory
        bpy.utils.register_class(options_panel_factory())
        return {'FINISHED'}
    
    
bpy.chocofur_refresh_time = 0
bpy.chocofur_category_time = 0
from bpy.app.handlers import persistent
@persistent
def chocofur_refresh(scene):
    newTime = time()
    if newTime > bpy.chocofur_refresh_time + 2:
        bpy.chocofur_refresh_time = newTime
        
        directory = bpy.context.window_manager.chocofur_model_manager.library_path
        if not os.path.exists(directory):
            print("Chocofur: Invalid path to model library")
            return
        # read file list from disk only when necessary
        dir_up = os.path.getmtime(directory)
        if dir_up > bpy.chocofur_category_time:
            bpy.chocofur_category_time = dir_up
            bpy.ops.chocofur.refresh_categories()
        
##################################################################
############################ Previews ############################
##################################################################
def enum_preview_items_func_factory(sname):
    def func(self, context):
        type = sname.lower()
        category = getattr(context.scene.chocofur_model_manager, sname+'_category')
        chocoprops = context.window_manager.chocofur_model_manager
        directory = os.path.join(chocoprops.library_path, sname, category, "renders")

        enum_items = []

        if context is None:
            return enum_items
        
        pcoll = collections[sname]["main"]
        
        if directory == pcoll.previews_dir:
            return pcoll.previews

        if directory and os.path.exists(directory):
            image_paths = []
            for fn in os.listdir(directory):
                if fn.lower().endswith(".jpg"):
                    image_paths.append(fn)

            for i, name in enumerate(image_paths):
                filepath = os.path.join(directory, name)
                
                if filepath in pcoll:
                    enum_items.append((name, name, "", pcoll[filepath].icon_id, i))
                else:
                    thumb = pcoll.load(filepath, filepath, 'IMAGE')
                    enum_items.append((name, name, "", thumb.icon_id, i))
        enum_items.sort()

        pcoll.previews = enum_items
        pcoll.previews_dir = directory
        return pcoll.previews
    
    return func

collections = {}


################################################################
############################ Append ############################
################################################################


####### Append Furniture ####### 

class OBJECT_OT_AddButton(bpy.types.Operator):
    bl_idname = "chocofur.add"
    bl_label = "Add Object"
    
    object_type = StringProperty()

    def execute(self, context):
        chocoprops = context.window_manager.chocofur_model_manager
        
        selected_preview = getattr(bpy.data.window_managers["WinMan"].chocofur_model_manager, self.object_type+"_previews")
        category = getattr(context.scene.chocofur_model_manager, self.object_type+"_category")
        model_path = self.object_type
        scn = bpy.context.scene
        
        filepath = (os.path.join(chocoprops.library_path, model_path, category, os.path.splitext(selected_preview)[0] + ".blend"))
        
        bpy.ops.object.select_all(action='DESELECT')        
        
        isLink = True if context.window_manager.chocofur_model_manager.import_mode == 'LINK' else False
        with bpy.data.libraries.load(filepath, isLink) as (data_from, data_to):
            data_to.objects = data_from.objects
            if data_from.groups:
                data_to.groups = data_from.groups
        
        '''
        for obj in data_to.objects:
            scn.objects.link(obj)
            obj.make_local()
            if not isLink and obj.data and obj.data.library:
                # make linked data-block single user copy in case of name clash
                obj.data = obj.data.copy()
            scn.update()
                
            obj.select = True
        '''       
            
        #blender bug when localising linked library: when Empty (parent) is made local parenting stops working
        #workaround: use DFS to localise objects in order from youngest to oldest
        parents = (ob for ob in data_to.objects if not ob.parent)
        def process_object(obj):
            for child in obj.children:
                process_object(child)
                                
            scn.objects.link(obj)
            obj.make_local()
            if not isLink and obj.data and obj.data.library:
                # make linked data-block single user copy in case of name clash
                obj.data = obj.data.copy()
            scn.update()                
            obj.select = True
                
        for obj in parents:
            process_object(obj)
                
        
        if context.window_manager.chocofur_model_manager.append_location == 'CURSOR':
            bpy.ops.transform.translate(value=context.scene.cursor_location, constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
               
        return{'FINISHED'}

############################ Register ############################

class Chocofur_Model_Manager_WM_Properties(bpy.types.PropertyGroup):
    library_path = StringProperty(
            name = "Library Path",
            subtype = 'DIR_PATH',
            get=lambda path: bpy.cmm_config_libpath,
            set=set_libpath,
            )
            
    append_location = EnumProperty(
            name="Append Location",
            description="Where model is to be appended",
            items = [
                    ("CENTER", "Center", "", 0),
                    ("CURSOR", "Cursor", "", 1),
                ],
            default = "CENTER",
            )
    import_mode = EnumProperty(
            name="Import Mode",
            description="Whether to Link or Append from library.",
            items = [
                    ("APPEND", "Append", "", 0),
                    ("LINK", "Link", "", 1),
                ],
            default = "APPEND",
            )

def populate_category_func_factory(category):
    def func(self, context):
        chocoprops = context.window_manager.chocofur_model_manager
        directory = os.path.join(chocoprops.library_path, category)
        
        if not os.path.exists(directory):
            print("Chokofur: Invalid path to model library")
            return []
        # read file list from disk only when necessary
        dir_up = os.path.getmtime(directory)
        if dir_up <= collections[category]['updated']:
            return collections[category]['items']
        
        mode_options = []
        counter=0
        for dir in (d for d in os.listdir(directory) if os.path.isdir(os.path.join(directory, d))):
            mode_options.append(
                (dir, dir, '', counter)
            )
            counter+=1
            
        collections[category]['items'] = mode_options
        collections[category]['updated'] = dir_up
        
        return mode_options
    return func

class Chocofur_Model_Manager_Scene_Properties(bpy.types.PropertyGroup):
    pass

def register():
    WindowManager.chocofur_model_manager = bpy.props.PointerProperty(type=Chocofur_Model_Manager_WM_Properties)
    bpy.types.Scene.chocofur_model_manager = bpy.props.PointerProperty(type=Chocofur_Model_Manager_Scene_Properties)
    
    for d in list_categories():
        setattr(Chocofur_Model_Manager_WM_Properties, d+'_previews', EnumProperty(
                items = enum_preview_items_func_factory(d)
        ))
                
        setattr(Chocofur_Model_Manager_Scene_Properties, d+'_category', EnumProperty(
            name=d+" Category",
            items=populate_category_func_factory(d),
            description="Select "+d,
        ))
        
        pcoll = bpy.utils.previews.new()
        pcoll.previews_dir = ""
        pcoll.previews = ()
        collections[d]={'main': pcoll, 'updated': 0}
        
        bpy.utils.register_class(cetegory_factory(d))
    from .gui import options_panel_factory
    bpy.utils.register_class(options_panel_factory())
    
    bpy.app.handlers.scene_update_post.append(chocofur_refresh)

def unregister():
    del WindowManager.chocofur_model_manager
    del bpy.types.Scene.chocofur_model_manager
    
    for key, val in collections.items():
        bpy.utils.previews.remove(collections[key]['main'])
        collections[key]['main'].clear()
        
    for c in categories_tmp:
        bpy.utils.unregister_class(getattr(bpy.types, "CATEGORY_PT_chocofur_"+c))
    bpy.utils.unregister_class(getattr(bpy.types, 'CATEGORY_PT_chocofur_OptionsPanel'))
    
        
    bpy.app.handlers.scene_update_post.remove(chocofur_refresh)
